﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimManager : MonoBehaviour
{
    Animator anim;
    public LayerMask mask = 8;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void WalkAnim()
    {
        anim.Play("Witch_Walk", 0);
    }
    public void WaitAnim()
    {
        anim.Play("Witch_Wait", 0);
    }
    public void DeathAnim()
    {
        anim.Play("Witch_Dead", 0);
    }
    public void DamageAnim()
    {
        anim.Play("Witch_Damage", 0);
    }
    public void AttackAnim()
    {
        anim.Play("Witch_Attack", 0);
    }
    [PunRPC]
    public void Penxue()
    {
        //GameObject px =  GameObject.FindGameObjectWithTag("Black");
        //px.tag = "Blue";
        //px.transform.SetParent(transform);
        //px.transform.position = transform.position;
       // StartCoroutine(Liuxue(px));
    }
    IEnumerator Liuxue(GameObject px)
    {
        yield return new WaitForSeconds(2f);
        px.tag = "Black";
    }
}
