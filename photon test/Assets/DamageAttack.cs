﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAttack : MonoBehaviour {
    // Use this for initialization
    public Transform[] mSwordHitablePoints = new Transform[3];
    void Start () {
    }

    private void Update()
    {
        RaycastHit hinfo;
        //int mask = 1 << LayerMask.NameToLayer("Defaut");// 只取与FoeHitReceiver层相交
        GameObject hitObj = null;
        if (Physics.Linecast(mSwordHitablePoints[0].position, mSwordHitablePoints[1].position, out hinfo))
        {
            hitObj = hinfo.collider.gameObject;
            Debug.Log(hitObj.name);
        }
        else if (Physics.Linecast(mSwordHitablePoints[1].position, mSwordHitablePoints[2].position, out hinfo))
        {
            hitObj = hinfo.collider.gameObject;
            Debug.Log(hitObj.name);
        }
        if (hitObj != null && hitObj.tag == "Player")
        {
            hitObj.transform.GetComponent<PlayerINFO>().HPDamage(50);
            Debug.Log(hitObj.name);
            PhotonView pv = hitObj.transform.GetComponent<PhotonView>();
            pv.RPC("Penxue", PhotonTargets.All);
        }
    }
}
    
