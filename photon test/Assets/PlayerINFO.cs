﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerINFO : MonoBehaviour, IPunObservable
{
    public bool isDead = false;
    public int place = 0;
    public int HP = 100;
    public int MP = 100;
    public int Kill = 0;
    public int Death = 0;
    public int realName = 65;
    int maxHP = 100, maxMP = 100;
    public Slider HPslider;
    public Slider MPslider;
    public Text realNameText;

    public int myname;
    public int team;//背面
    void Start()
    {
        //team = PlayerMSG.instance.team;
        myname = Random.Range(10000, 99999);
        this.gameObject.name = myname.ToString();
        StartCoroutine(RefreshMP());
    }
    IEnumerator RefreshMP()
    {
        while (true)
        {
            if (MP < maxMP)
            {
                MP++;
            }
            yield return new WaitForSeconds(3f);
        }
    }
    void Update()
    {
        HPslider.GetComponent<Slider>().value = (float)HP / maxHP;
        MPslider.GetComponent<Slider>().value = (float)MP / maxMP;
        realNameText.text = ((char)realName).ToString();
        //Debug.Log(transform.rotation);
        // if (!photonView.isMine)
        //   {
        // transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
        //  transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        //  }
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(isDead);
            stream.SendNext(team);
            stream.SendNext(realName);
            stream.SendNext(this.HP);
            stream.SendNext(this.MP);
            stream.SendNext(this.myname);
            stream.SendNext(this.Kill);
            stream.SendNext(this.Death);
            //stream.SendNext(transform.position);
            //stream.SendNext(transform.rotation);
        }
        else
        {
            this.isDead = (bool)stream.ReceiveNext();
            this.team = (int)stream.ReceiveNext();
            realName = (int)stream.ReceiveNext();
            this.HP = (int)stream.ReceiveNext();
            this.MP = (int)stream.ReceiveNext();
            this.myname = (int)stream.ReceiveNext();
            this.Kill = (int)stream.ReceiveNext();
            this.Death = (int)stream.ReceiveNext();


            this.gameObject.name = myname.ToString();
            //correctPlayerPos = (Vector3)stream.ReceiveNext();
            //correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }
    //private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    // private Quaternion correctPlayerRot = Quaternion.identity;
    [PunRPC]
    public void HPDamage(int i)
    {
        HP -= i;
        if (HP <= 0)
        {
            isDead = true;
            GetComponent<AnimManager>().DeathAnim();
            StartCoroutine(Refresh());
        }
        else if (i > 0)
        {
            GetComponent<AnimManager>().DamageAnim();
        }
    }
    public void MPDamage(int i)
    {
        MP -= i;
    }
    void HPMPRefresh()
    {
        if (HP <= 0)
        {
            HP = maxHP;
            MP = maxMP;
        }
    }
    [PunRPC]
    void Killadd(int i)
    {
        Kill += i;
    }
    [PunRPC]
    void SetTeam(int i)
    {
        team = i;
    }
    [PunRPC]
    void Deathadd()
    {
        Death++;
    }
    IEnumerator Refresh()
    {
        //if (photonView.isMine)
        // {
        bool swit = false;
        if (GetComponent<LineManager>().enabled == true)
        {
            GetComponent<LineManager>().enabled = false;
            GetComponent<AnimManager>().enabled = false;
            swit = true;
        }
       // }
        yield return new WaitForSeconds(1f);
        //this.transform.position = new Vector3(0, -100f, 0);
        yield return new WaitForSeconds(4f);
        if (place == 0)
        {
            this.transform.position = new Vector3(0f, 0.5f, 0f);
        }
        else
        {
            this.transform.position = new Vector3(0f, -0.5f, 0f);
            //transform.Rotate(new Vector3(180, 180, 0));
        }
        // if (photonView.isMine)
        // {
        if (swit)
        {
            GetComponent<LineManager>().enabled = true;
            GetComponent<AnimManager>().enabled = true;
        }
        //}
        isDead = false;
        HPMPRefresh();
        GetComponent<AnimManager>().WaitAnim();
    }
}
