﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAINNetworkTest : MonoBehaviour
{
    static public MAINNetworkTest instance;
    public GameObject player0;
    public GameObject player1;
    // Use this for initialization
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnLevelWasLoaded(int level)
    {

        if (PlayerMSG.instance.PlayPro == "2")
        {
            PhotonNetwork.Instantiate(player0.name, new Vector3(Random.Range(-4, 4), 0f, Random.Range(-4, 4)), new Quaternion(0, 0, 0, 0), 0);
        }
        else if (PlayerMSG.instance.PlayPro == "3")
        {
            PhotonNetwork.Instantiate(player1.name, new Vector3(Random.Range(-4, 4), 0f, Random.Range(-4, 4)), new Quaternion(0, 0, 0, 0), 0);
        }
        Debug.LogError(PlayerMSG.instance.teamId);
        if (PlayerMSG.instance.teamId >= 2)
        {
            GameObject.FindGameObjectWithTag("Player").transform.GetComponent<LineManager>().Fanzhuan();
        }
        PhotonView pv = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<PhotonView>();
        pv.RPC("SetTeam", PhotonTargets.All, PlayerMSG.instance.teamId);
        
        //GameObject.FindGameObjectWithTag("Player").transform.SetParent(GameObject.FindGameObjectWithTag("ZDown").transform);
    }
}
