﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Page_Menu : MonoBehaviour
{

    public Button btn_logo;
    public Button btn_play;

    public Transform menuPos_1;
    public Transform menuPos_2;

    public GameObject[] btn = new GameObject[4];

    public void Start()
    {
        btn_logo.onClick.AddListener(OnClickMenu);
        btn_play.onClick.AddListener(PlayGame);
        menuPos_2.gameObject.SetActive(false);
        btn_logo.gameObject.transform.position = menuPos_1.transform.position;
    }

    public void OnClickMenu()
    {
        btn_logo.gameObject.transform.DOMove(menuPos_2.transform.position, 0.3f);
        menuPos_2.gameObject.SetActive(true);
    }

    public void PlayGame()
    {
        menuPos_2.gameObject.SetActive(false);
        btn_logo.gameObject.transform.DOMove(menuPos_1.transform.position, 0.3f);
    }
    public void OnClickCharacter0()
    {
        btn[0].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "0";
    }
    public void OnClickCharacter1()
    {
        btn[1].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "1";
    }
    public void OnClickCharacter2()
    {
        btn[2].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "2";
    }
    public void OnClickCharacter3()
    {
        btn[3].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "3";
    }
     
}
