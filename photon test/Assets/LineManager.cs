﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LineManager : MonoBehaviour
{
    public LayerMask mask;
    public List<Vector3> L3;
    public List<Vector3> L2;
    public List<Vector3> L1;
    bool isMove = false;
    // Use this for initialization
    void Start()
    {
        //transform.rotation = Quaternion.Euler(new Vector3(1, 0, 1));
        //transform.forward = new Vector3(1, 0, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && isMove == false)
        {

            //Debug.Log(Input.mousePosition);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, int.MaxValue, mask))
            {
                //Debug.Log(hit.point);
                if (GetComponent<PlayerINFO>().place == 0)
                {
                    L3.Add(hit.point + new Vector3(0, transform.localScale.y / 2, 0));
                }
                else
                {
                    L3.Add(hit.point + new Vector3(0, -transform.localScale.y / 2, 0));
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && isMove == false)
        {
            TransPoint2();
            TransPoint1();
            StartCoroutine(OnMove());
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log(123);
            transform.position = new Vector3(transform.position.x, -transform.position.y, transform.position.z);
            transform.Rotate(new Vector3(180, 0, 0));
            GetComponent<PlayerINFO>().place = 1 - GetComponent<PlayerINFO>().place;
            GameObject bl = GameObject.FindGameObjectWithTag("GameController");

            Debug.Log(bl.transform.name);
            bl.transform.Rotate(new Vector3(180, 0, 0));
        }
    }
    public void TransPoint2()
    {
        int len = L3.Count;
        L2.Add(L3[0]);
        for (int i = 0; i < len - 1; i++)
        {
            if ((L3[i + 1] - L3[i]).magnitude == 0)
            {
                continue;
            }
            L2.Add(L3[i + 1]);
        }
        L3.Clear();
    }
    public void TransPoint1()
    {
        int len = L2.Count;
        for (int i = 0; i < len - 10; i = i + 10)
        {
            Vector3 avn = Vector3.zero;
            for (int j = 0; j < 10; j++)
            {
                avn += L2[i + j];
            }
            L1.Add(avn / 10);
        }
        L1.Add(L2[L2.Count - 1]);
        L2.Clear();
    }
    IEnumerator OnMove()
    {
        isMove = true;
        //transform.DOMove(L1[0], 0.01f);//.SetEase(Line);
        int len = L1.Count;
        for (int i = 0; i < len; i++)
        {
            yield return new WaitForSeconds(0.01f);
            if (i < len - 1)
            {
                transform.forward = L1[i + 1] - L1[i];
                //transform.LookAt(Vector3.Lerp(L1[i + 1],L1[i+2],1f));
            }
            transform.DOMove(L1[i], 0.01f);//.SetEase(Line);
        }
        L1.Clear();
        isMove = false;
    }
}*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LineManager : MonoBehaviour
{
    public LayerMask mask;
    public List<Vector3> L3;
    public bool isMove = false;
    public Transform[] mSwordHitablePoints = new Transform[3];
    public bool isBack = false;
    public Image imageBack;
    public Text textBack;
    private float old_y = 0;
    //记录当前的重力感应的Y值
    private float new_y;
    //当前手机晃动的距离
    private float currentDistance = 0;

    //手机晃动的有效距离
    private float distance = 0.2f;

    public float backTran = 2;
    // Use this for initialization
    void Start()
    {
        //transform.rotation = Quaternion.Euler(new Vector3(1, 0, 1));
        //transform.forward = new Vector3(1, 0, 1);
        StartCoroutine(refreshBackTran());
    }
    IEnumerator refreshBackTran()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            {
                imageBack.GetComponent<Image>().fillAmount = (backTran - (int)backTran);
                if (backTran < 3)
                    backTran = backTran + 0.001f;
            }
        }
    }

    // Update is called once per frame
    IEnumerator BackRotate()
    {
        yield return new WaitForSeconds(0.1f);
        isBack = false;
    }
    public void Fanzhuan()
    {
        transform.position = new Vector3(transform.position.x, -transform.position.y, transform.position.z);
        transform.Rotate(new Vector3(180, 180, 0));
        Debug.Log(transform.rotation);
        GetComponent<PlayerINFO>().place = 1 - GetComponent<PlayerINFO>().place;
        GameObject bl = GameObject.FindGameObjectWithTag("GameController");
        bl.transform.Rotate(new Vector3(180, 180, 0));
        MirrorFlipCamera.flipHorizontal = !MirrorFlipCamera.flipHorizontal;
    }
    void Update()
    {
        if (backTran > 1)
        {
            if(backTran == 3)
            {
                StopCoroutine(refreshBackTran());
            }
            new_y = Input.acceleration.y;
            currentDistance = new_y - old_y;
            old_y = new_y;
            if ((currentDistance > distance || Input.GetKeyDown(KeyCode.Space)) && isBack == false)
            {
                if (backTran == 3)
                {
                    StartCoroutine(refreshBackTran());
                }
                backTran--;
                isBack = true;
                StartCoroutine(BackRotate());
                Fanzhuan();
            }
        }
        textBack.text = backTran.ToString();
        RaycastHit hinfo;
        GameObject hitObj = null;
        if (Physics.Linecast(mSwordHitablePoints[0].position, mSwordHitablePoints[1].position, out hinfo))
        {
            hitObj = hinfo.collider.gameObject;
            Debug.LogError(hitObj.name);
        }
        else if (Physics.Linecast(mSwordHitablePoints[1].position, mSwordHitablePoints[2].position, out hinfo))
        {
            hitObj = hinfo.collider.gameObject;
            Debug.LogError(hitObj.name);
        }
        if (hitObj != null && hitObj.tag == "Player")
        {
            PhotonView pv = hitObj.transform.GetComponent<PhotonView>();
            pv.RPC("HPDamage", PhotonTargets.All,50);
            if(hitObj.transform.GetComponent<PlayerINFO>().HP == 0)
            {
                PhotonView pvSelf = transform.GetComponent<PhotonView>();
                pvSelf.RPC("Killadd", PhotonTargets.All, 1);
                pv.RPC("Deathadd", PhotonTargets.All);
            }
        }
        else if(hitObj != null && hitObj.tag == "Weapon")
        {
            //Debug.LogError("pingdao");
            PhotonView pv = transform.GetComponent<PhotonView>();
            pv.RPC("PlayMusic", PhotonTargets.All);
        }
       


        if (Input.GetMouseButton(0) && isMove == false)
        {

            //Debug.Log(Input.mousePosition);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, int.MaxValue, mask))
            {
                if (GetComponent<PlayerINFO>().place == 0)
                {
                    L3.Add(hit.point + new Vector3(0, 0, 0));//transform.localScale.y / 2, 0));
                }
                else
                {
                    L3.Add(hit.point + new Vector3(0, 0, 0));//-transform.localScale.y / 2, 0));
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && isMove == false)
        {
            StartCoroutine(OnMove());
        }
    }
    IEnumerator OnMove()
    {
       // GetComponent<AnimManager>().WalkAnim();
        isMove = true;
        int len = L3.Count;
        for(int i = 0;i< len - 1; i++)
        {
            if ((L3[i+1]-L3[i]).magnitude <= 0.2f)
            {
                continue;
            }
            yield return new WaitForSeconds(0.01f);
            transform.forward = L3[i + 1] - L3[i];
           
            if (GetComponent<PlayerINFO>().place == 1)
            {
                Debug.Log(transform.rotation);
                transform.Rotate(new Vector3(180, 180, 0));
            }
            transform.DOMove(L3[i], 0.01f);//.SetEase(Line);
        }
        L3.Clear();
        isMove = false;
        //GetComponent<AnimManager>().WaitAnim();
    }
}
