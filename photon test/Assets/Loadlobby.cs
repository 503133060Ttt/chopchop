﻿using ExitGames.Client.Photon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Loadlobby : Photon.PunBehaviour
{
    private int maxNumPlayerPerRoom = 4;
    public Mutex mutex;
    private PhotonPeer peer;
    static public int ID = 0;

    static public string room = "room";
    public Text context;
    // public string[] a = { "adam", "bill" };
    // Use this for initialization
    private void Awake()
    {
        PhotonNetwork.PhotonServerSettings.AppID = "00c931a2-d860-4946-95f2-8f80849356f5";
    }
    void Start()
    {

        //PhotonNetwork.ConnectUsingSettings("0.0.1");
        PhotonNetwork.ConnectToRegion(CloudRegionCode.cn, "0.0.2");
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void StartMatchingClick()
    {
        PhotonNetwork.playerName = UnityEngine.Random.Range(1, 100000).ToString();
        ID = ID + 1;
        if (ID % maxNumPlayerPerRoom != 0)
        {
            room = room + ID;
        }
        //  PhotonNetwork.JoinOrCreateRoom(room, new RoomOptions { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
        PhotonNetwork.JoinRandomRoom();
    }
    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        //       //PhotonNetwork.CreateRoom(Random.Range(1, 100000).ToString(), roomOptions, TypedLobby.Default);
        //int number = PhotonNetwork.playerList.Length;
        //Debug.Log("OnPhotonJoinRoomFailed numbers " + number + " room " + room);
        //if (number % maxNumPlayerPerRoom != 0)
        //{
        //    //           mutex.WaitOne();
        //    ID = ID + 1;
        //    room = room + ID;
        //    //           mutex.ReleaseMutex();
        //}
        //Debug.Log("OnPhotonJoinRoomFailed numbers " + number + " room " + room);
        //PhotonNetwork.CreateRoom(room, new RoomOptions() { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        int ID = RandomNumber(1, 100000);
        PhotonNetwork.CreateRoom(room + ID, new RoomOptions() { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
    }
    public override void OnJoinedRoom()
    {
        context.text = "waiting for match";
        Debug.Log("join room " + room);
        PhotonNetwork.automaticallySyncScene = true;
        //base.OnJoinedRoom();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        int number = PhotonNetwork.playerList.Length;
        //Debug.Log("OnPhotonPlayerConnected numbers" + number + "room" + room);
        //if (number % maxNumPlayerPerRoom != 0)
        //{
        //    //           mutex.WaitOne();
        //    ID = ID + 1;
        //    room = room + ID;
        //    //           mutex.ReleaseMutex();
        //}
        PlayerMSG.instance.teamId += 1;
        if (number % maxNumPlayerPerRoom == 0)
        {
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.LoadLevel("Launcher");
            }
        }
    }

    public static int RandomNumber(int min, int max)
    {
        System.Random random = new System.Random();
        return random.Next(min, max);
    }

    public void OnDestroy()
    {
        Destroy(this.gameObject);
    }
}
